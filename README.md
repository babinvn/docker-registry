# docker-registry

Private docker registry deployment step-by-step guide

## Create docker machine

I use [Hetzner Cloud](https://www.hetzner.com/cloud) to host the docker
registry.

```
# docker-machine rm registry
docker-machine create \
--driver=hetzner \
--hetzner-api-token=$HETZNER_TOKEN \
--hetzner-server-type=cx11 \
--hetzner-server-location=fsn1 \
registry
```

After docker machine is created update it to the latest package version:

```
docker-machine ssh registry
```

then

```
apt-get update
apt-get dist-upgrade
reboot
```

Create a volume (start with a small one -- 20GB) and attach it to the server.
Create a `docker` directory and reflink it to `/var/docker`:

```
mkdir /mnt/HC_Volume_6701654/docker
ln -s /mnt/HC_Volume_6701654/docker /var/
```

## Create SSL certificate(s)

Create domain names for your registry server (mine is `registry.bvn.me`) and your
authentication server (mine is `auth.bvn.me`).

We need either a wildcard SSL or two different SSL certificates to match your
domain names. This project assumesthat wildcard certificate is used. To create
a [Letsencrypt](https://letsencrypt.org) wildcard SSL certificate using
[Cloudflare](https://cloudflare.com/) DNS verification provide your Cloudflare
credentials:

```
mkdir -p /var/docker/nginx/conf.d
echo "# Cloudflare API credentials used by Certbot" >> /var/docker/nginx/conf.d/.cf
echo "dns_cloudflare_email = ${dns_cloudflare_email}" >> /var/docker/nginx/conf.d/.cf
echo "dns_cloudflare_api_key = ${dns_cloudflare_api_key}" >> /var/docker/nginx/conf.d/.cf
chmod 0600 /var/docker/nginx/conf.d/.cf
```

The issue the certificate:

```
docker run -it --rm \
-v /var/docker/nginx/conf.d:/etc/letsencrypt certbot/dns-cloudflare certonly \
-d bvn.me -d '*.bvn.me' -m admin@bvn.me --quiet \
--agree-tos \
--dns-cloudflare \
--dns-cloudflare-credentials /etc/letsencrypt/.cf \
--dns-cloudflare-propagation-seconds 30
```

Creating non-wildcard SSL certificates is easier as no Cloudflare credentials
are required:

```
docker run -it --rm -p 80:80 \
-v /var/docker/nginx/conf.d:/etc/letsencrypt \
certbot/certbot certonly --standalone -d registry.bvn.me -m admin@bvn.me

docker run -it --rm -p 80:80 \
-v /var/docker/nginx/conf.d:/etc/letsencrypt \
certbot/certbot certonly --standalone -d auth.bvn.me -m admin@bvn.me
```

## Authentication server

Install `htpasswd` command then generate your registry user password:

```
apt install apache2-utils
htpasswd -nB <username>
```

Create the auth server configuration file `/var/docker/auth/config/auth_config.yml`:

```
mkdir -p /var/docker/auth/config/
touch /var/docker/auth/config/auth_config.yml
```

Edit `auth_config.yml` to match your certificates, username and password.

## Nginx

Put `default.conf` and `registry.conf` files to Nginx configuration folder
`/var/docker/nginx/conf.d` (edit to match your domain name and certificates).

## Deploy

Edit `docker-container.yml` and run to deploy:

```
docker-container up -d
```

Test your setup by running login command (use you registry host name):

```
docker login registry.bvn.me

```

Create a test image and push to the registry:

```
cd test

docker build -t test-tomcat .

docker tag test-tomcat registry.bvn.me/tests/test-tomcat

docker push registry.bvn.me/tests/test-tomcat
```

To diagnose problems start by reading container logs first:

```
docker log registry

docker log auth

docker log nginx
```

Hope this helps!
